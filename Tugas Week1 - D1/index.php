<?php

spl_autoload_register(function ($class_name) {
    include_once $class_name . '.php';
});

$maung      = new Harimau("Maung");
$heulang    = new Elang("Heulang");

echo "============= <b>Pertandingan Hewan</b> =====================<br>";
echo $maung->nama." Melawan ".$heulang->nama."<br>";
echo "<br>";

echo "<b>Informasi Hewan yang Bertanding</b><br>";
echo "Hewan Pertama<br>";
$maung->getInfoHewan();
echo "<br>";
echo "Hewan Kedua<br>";
$heulang->getInfoHewan();
echo "<br>";

echo "<b>Atraksi</b><br>";
echo $maung->atraksi() . "<br>";
echo $heulang->atraksi();
echo "<br><br>";

echo "<b>Timeline Pertandingan</b><br>";
while($maung->darah>0 && $heulang->darah>0) {
    echo $maung->serang($heulang) . "<br>";
    echo $heulang->diserang($maung);
    if ($heulang->darah >0 ){
        echo "! Sisa darah " . $heulang->darah . "<br>";
    } else {
        echo "<br><b>".$heulang->nama." Kalah</b><br>";
        break;
    }
    echo $heulang->serang($maung) . "<br>";
    echo $maung->diserang($heulang);
    if ($maung->darah >0 ){    
        echo "! Sisa darah " . $maung->darah . "<br>";
    } else {
        echo "<br><b>".$maung->nama." Kalah</b><br>";
    }
}