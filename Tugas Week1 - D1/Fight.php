<?php

trait Fight {
    public $attackPower, $defencePower;

    public function diserang($musuh){
        $this->darah -= $musuh->attackPower/$this->defencePower;
        return "{$this->nama} sedang diserang";
    }

    public function serang($target){
        return "{$this->nama} sedang menyerang {$target->nama}";
    }

}