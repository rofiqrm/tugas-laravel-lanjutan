<?php

class Elang {
    use Hewan, Fight;
    public function __construct($nama) {
        $this->nama = $nama;
        $this->jumlahKaki = 2;
        $this->keahlian = "terbang tinggi";
        $this->attackPower = 10; 
        $this->defencePower = 5;
    }
    public function getInfoHewan() {
        echo "Nama: " . $this->nama . "<br>";
        echo "Jenis Hewan: Elang<br>";
        echo "Jumlah kaki: " . $this->jumlahKaki . "<br>";
        echo "Keahlian: " . $this->keahlian . "<br>";
        echo "Attack Power: " . $this->attackPower . "<br>";
        echo "Defence Power: " . $this->defencePower . "<br>";
    }

}