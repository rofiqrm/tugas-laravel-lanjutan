<?php

class Harimau {
    use Hewan, Fight;
    public function __construct($nama) {
        $this->nama = $nama;
        $this->jumlahKaki = 4;
        $this->keahlian = "lari cepat";
        $this->attackPower = 7; 
        $this->defencePower = 8;
    }
    public function getInfoHewan() {
        echo "Nama: " . $this->nama . "<br>";
        echo "Jenis Hewan: Harimau<br>";
        echo "Jumlah kaki: " . $this->jumlahKaki . "<br>";
        echo "Keahlian: " . $this->keahlian . "<br>";
        echo "Attack Power: " . $this->attackPower . "<br>";
        echo "Defence Power: " . $this->defencePower . "<br>";
    }

}