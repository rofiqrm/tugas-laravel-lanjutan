<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class userMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->route()->uri()=='superadmin')
        {
            if(Auth::user()->Role() =='superadmin'){
                return $next($request);
            }else{
                abort(403);
            }
        }
        elseif($request->route()->uri()=='admin')
        {
            if(Auth::user()->Role() == 'admin' || Auth::user()->Role() == 'superadmin')
            {
                return $next($request);
            }else{
                abort(403);
            }
        }
        else{
            return $next($request);
        }
    }
}
