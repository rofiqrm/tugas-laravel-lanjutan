<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class UserController extends Controller
{
    public function Superadmin() {
        return view('user.route1');
    }

    public function Admin() {
        return view('user.route2');
    }

    public function Guest() {
        return view('user.route3');
    }
}
